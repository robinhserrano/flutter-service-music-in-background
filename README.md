# Flutter Service - Play Music in Background

## Setup Guide

This section focuses on the Step-by-step instruction to clone the project locally and install the needed Flutter packages.

1. Open GitBash or Terminal in your computer.
2. Change the current working directory to the location where you want the cloned directory.
3. Type `git clone`, and then paste the URL you copied earlier. `git clone <project_url>`.
4. Navigate to the flutter project by using the command `cd flutter_service_music_in_background`.
5. Type `flutter pub get` to install the required packages in `pubspec.yaml` to your device.
6. Type `flutter run --no-sound-null-safety` to start building or running the cloned flutter application. (Note: Dart and Flutter SDK must be already installed in your computer, otherwise follow the instructions in the provided [link](https://flutter.dev/docs/get-started/install/windows)).

# Installed Packages

The following are the packages installed in pubspec.yaml with their respective description: 

- [**assets_audio_player**](https://pub.dev/packages/assets_audio_player): ^2.0.14- Play music/audio stored in assets files (simultaneously) directly from Flutter (android / ios / web / macos). You can also use play audio files from network using their url, radios/livestream and local files

## Screens

![Screenshot_1632474636.png](Flutter%20Service%20-%20Play%20Music%20in%20Background%202771a3bf8c4247849c7be3083fdf0a03/Screenshot_1632474636.png)

![Screenshot_1632474641.png](Flutter%20Service%20-%20Play%20Music%20in%20Background%202771a3bf8c4247849c7be3083fdf0a03/Screenshot_1632474641.png)

![Screenshot_1632475641.png](Flutter%20Service%20-%20Play%20Music%20in%20Background%202771a3bf8c4247849c7be3083fdf0a03/Screenshot_1632475641.png)